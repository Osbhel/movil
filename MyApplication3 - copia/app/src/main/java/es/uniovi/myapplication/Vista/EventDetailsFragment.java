package es.uniovi.myapplication.Vista;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import es.uniovi.myapplication.R;
import es.uniovi.myapplication.Modelo.Event;
import es.uniovi.myapplication.ViewModel.EventsListViewModel;

public class EventDetailsFragment extends Fragment implements View.OnClickListener {
    private static final String DESCRIPTION_ARG = "description";
    EventsListViewModel eventsListViewModel;
    TextView name, teacher, description;

    //Es el fragmento correspondiente a la activdad detalls en este fragmento se
    // muestran los datos  que se han clicado antes de forma mas detallada
    public static EventDetailsFragment newInstance(String desc) {

        EventDetailsFragment fragment = new EventDetailsFragment();

        Bundle args = new Bundle();
        args.putString(DESCRIPTION_ARG, desc);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View viewRoot;
        String desc = " ";
        Bundle args = getArguments();
        viewRoot = inflater.inflate(R.layout.fragment_details_event, container,false);
        //Comprabmos si nos han enviado argumentos si no no pintamso nada esto se realiza para
        // evitar que aparezcan los titulos de lso datos aunque no se haya clicado nada
        if (args != null) {
            desc = args.getString(DESCRIPTION_ARG);
            viewRoot.findViewById(R.id.textView).setVisibility(View.VISIBLE);
            viewRoot.findViewById(R.id.textView3).setVisibility(View.VISIBLE);
            viewRoot.findViewById(R.id.textView5).setVisibility(View.VISIBLE);
            viewRoot.findViewById(R.id.button).setVisibility(View.VISIBLE);

            name = viewRoot.findViewById(R.id.details_name);
            teacher = viewRoot.findViewById(R.id.details_teacher);
            description = viewRoot.findViewById(R.id.details_description);
            Button boton= viewRoot.findViewById(R.id.button);
            boton.setOnClickListener(this);

            eventsListViewModel = ViewModelProviders.of(this).get(EventsListViewModel.class);
            eventsListViewModel.getEvent().observe(this, new Observer<Event>() {

                @Override
                public void onChanged(@Nullable Event event) {
                    assert event != null;
                    name.setText(event.getTitulo());
                    teacher.setText(event.getCategoria());
                    description.setText(event.getDescription());
                }
            });
            eventsListViewModel.setName(desc);
        }




        return viewRoot;
    }

    @Override
    public void onClick(View view) {
        //Extraemos la latitud y longitud  del evento seleccionado y lanzamos la actividad de mapas
        Intent intent = new Intent(getContext(), MapsActivity.class);
        Event prueba = eventsListViewModel.getEvent().getValue();
        double latitud = prueba.getLatitud();
        double longitud = prueba.getLongitud();
        intent.putExtra("Latitud", latitud);
        intent.putExtra("Longitud", longitud);
        startActivity(intent);

    }
}
