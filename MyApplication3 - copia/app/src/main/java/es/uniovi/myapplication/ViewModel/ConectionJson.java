package es.uniovi.myapplication.ViewModel;


import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.ListIterator;

import es.uniovi.myapplication.Modelo.Event;
import es.uniovi.myapplication.Modelo.Parametro;
import es.uniovi.myapplication.Modelo.Parametros;

public class ConectionJson {
    private RequestQueue requestQueue;
    private String URL;
    private EventNamesViewModel eventNamesViewModel;
    //Clase creada para la conexion con la red en esta clase se descargan los
    // datos y se introducen en la base de datoso
    public ConectionJson(Context cont, String url, EventNamesViewModel eventNamesViewModel){
        this.requestQueue = Volley.newRequestQueue(cont);
        this.URL = url;
        this.eventNamesViewModel = eventNamesViewModel;
    }

    private JsonObjectRequest CreationJson(){
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject datos = response.getJSONObject("results");
                    Gson gson= new Gson();
                    Parametros listaparametros = gson.fromJson(datos.toString(),Parametros.class);
                    ArrayList<Parametro> prueba= listaparametros.getEventos();
                    ListIterator<Parametro> itr = prueba.listIterator();
                    //Se crea un id por si existen elementos con el mismo titulo
                    int id =0;
                    //Se borra la base de datos antes de la descarga
                    eventNamesViewModel.delete();
                    while(itr.hasNext()){
                        Parametro evn = itr.next();
                        Event event = new Event( id, evn.getTitulo(), evn.getCategoria(), evn.getDescripcion(),
                                    evn.getSeccion(), evn.getLat(), evn.getLong(), evn.getHorario());
                        eventNamesViewModel.insert(event);
                        id++;
                    }

                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        return jsonObjectRequest;
    }

    public void conection(){
        JsonObjectRequest request = CreationJson();
        requestQueue.add(request);
    }

}
