package es.uniovi.myapplication.ViewModel;


import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

import es.uniovi.myapplication.Modelo.Event;

public class EventNamesViewModel extends AndroidViewModel {
    private EventRepository mRepository;
    private LiveData<List<String>> mNames;


    public EventNamesViewModel(@NonNull Application application) {
        super(application);
        mRepository = new EventRepository(application);
        mNames = mRepository.getEventNames();
    }

    public LiveData<List<String>> getEventsNames(){
        return mNames;
    }

    public LiveData<List<String>> getEventFiltro(String name){ return mRepository.getEventByFiltro(name);}

    public void insert(Event event){
        mRepository.insertEvent(event);
    }

    public void delete(){mRepository.deleteAll();}

}
