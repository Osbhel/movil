package es.uniovi.myapplication.Modelo;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import es.uniovi.myapplication.Modelo.Parametro;

//Clase que nos posibilita hacer la deseriazliacion del json de forma comoda
public class Parametros {
    public ArrayList<Parametro> getEventos() {
        return eventos;
    }

    public void setEventos(ArrayList<Parametro> eventos) {
        this.eventos = eventos;
    }

    @SerializedName("bindings")
    ArrayList<Parametro> eventos;

    public Parametros(ArrayList<Parametro> eventos){
        this.eventos= eventos;
    }
}
