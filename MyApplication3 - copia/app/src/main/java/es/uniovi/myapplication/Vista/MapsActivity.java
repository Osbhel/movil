package es.uniovi.myapplication.Vista;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import es.uniovi.myapplication.R;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    double longitud;
    double latitud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        //Extraemos los valores enviados por la anteriror actividad del bundle
        longitud= getIntent().getExtras().getDouble("Longitud");
        latitud= getIntent().getExtras().getDouble("Latitud");
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        UiSettings  settings = mMap.getUiSettings();
        settings.setZoomControlsEnabled(true);
        //Aumentamos el zoom para ver mejor la zona
        mMap.setMinZoomPreference(16);

        // Introducimos los valores que hemos extraido del bundle y posicionamos
        // una marca en el sitio en cuestion
        LatLng sydney = new LatLng(latitud,longitud);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }
}
