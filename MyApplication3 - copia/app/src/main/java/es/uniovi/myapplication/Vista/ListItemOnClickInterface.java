package es.uniovi.myapplication.Vista;


public interface ListItemOnClickInterface {
    public void onItemClick(String name);
}
