package es.uniovi.myapplication.Vista;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import es.uniovi.myapplication.R;
import es.uniovi.myapplication.ViewModel.ConectionJson;
import es.uniovi.myapplication.ViewModel.EventNamesViewModel;


public class EventListFragment extends Fragment {
    //Es el fragmento correspondiente a la actividad Main se encarga de listar lso elementos

    private EventListAdapter adapter = null;
    public final String URL = "http://opendata.caceres.es/GetData/GetData?" +
            "dataset=event:Event&format=json";
    EventNamesViewModel eventNamesViewModel;

    public static int posicion = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new EventListAdapter(getContext());
        eventNamesViewModel = ViewModelProviders.of(this).get(EventNamesViewModel.class);
        //Comprabamos si es la primaera vez que se lanza la aplicacion y descargamos los datos y
        // los introducimos en la base de datos
        if (savedInstanceState==null) {
            ConectionJson conexion = new ConectionJson(getContext(), URL, eventNamesViewModel);
            conexion.conection();
        }

        eventNamesViewModel.getEventsNames().observe(this, new Observer<List<String>>() {
            @Override
            public void onChanged(@Nullable List<String> events) {
                adapter.setNames(events);
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);


    }

    public void Filtrar(String tipo){
        //Realizamos el filtro especificado en los argumentos y lo mostramos
        eventNamesViewModel = ViewModelProviders.of(this).get(EventNamesViewModel.class);
        eventNamesViewModel.getEventFiltro(tipo).observe(this, new Observer<List<String>>() {
            @Override
            public void onChanged(@Nullable List<String> events) {
                adapter.setNames(events);
            }
        });

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView;
        rootView = inflater.inflate(R.layout.activity_main, container, false);
        RecyclerView recyclerView =rootView.findViewById(R.id.recyclerview);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

        return rootView;
    }




}
