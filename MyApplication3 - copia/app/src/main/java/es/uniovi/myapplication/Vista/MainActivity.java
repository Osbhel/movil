package es.uniovi.myapplication.Vista;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import es.uniovi.myapplication.R;

public class MainActivity extends AppCompatActivity implements ListItemOnClickInterface {

    FragmentManager fragmentManager;
    EventListFragment fragment;
    EventDetailsFragment detailsfragment;
    Toolbar toolbar;

    boolean mTwoPanes = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Comprobacion del color de la toolbar si se encuentra en shredpreferences lo cmabiamos
        SharedPreferences shPref = this.getPreferences(Context.MODE_PRIVATE);
        int color = shPref.getInt("color",100);

        if(color!=100){
            toolbar.setBackgroundColor(color);
        }
        //Creacion del fragmento de la lista
        fragmentManager = getSupportFragmentManager();
        fragment = (EventListFragment)
                fragmentManager.findFragmentById(R.id.event_list_frag);
        mTwoPanes = getResources().getBoolean(R.bool.doble);

    }

    @Override
    protected void onPause() {
        super.onPause();
        //Eliminamos el fragmento si es distinto de null para poder arreglar el error del giro
        if(detailsfragment!=null)
            getSupportFragmentManager().beginTransaction().remove(detailsfragment).commit();
    }

    //Cuando llamamos a filtrar nos retorna el valor seleccionado en la pestaña filtro y realizamos
    // el filtro por ese valor
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                String filtro = data.getExtras().getString("Filtro");
                fragment.Filtrar(filtro);
            }
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Manejador de si algun elemento de la toolbar ha sido seleccionado
        int id = item.getItemId();

        //Este primer elemento solo cambia el color de la toolbar y lo guarda en shredpreferences
        if (id == R.id.action_settings) {
            toolbar.setBackgroundColor(Color.BLACK);
            SharedPreferences shPref = this.getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor edit = shPref.edit();
            edit.putInt("color",Color.BLACK);
            edit.commit();
        }
        //El segundo elemento nos lleva a la actividad filtro para poder selecionador
        // la categoria por la que filtramos
        if (id== R.id.action_filter){
            Intent intent = new Intent(this, FilterActivity.class);
            startActivityForResult(intent,1);
        }

        return true;
    }



    @Override
    public void onItemClick(String name) {
        //Comprueba en que posicion se encuentra en movil si es en modo normal lanza la
        // activdad detalless
        if ( !mTwoPanes ) {
            Intent intent = new Intent(this, DetailsEventActivity.class);
            intent.putExtra(DetailsEventActivity.NAME_DETAIL, name);
            startActivity(intent);
        }
        //Si se encuentra en modo apaisado lanza los fragmentos uno al lado del otro
        else{
             detailsfragment =
                    EventDetailsFragment.newInstance(name);

            fragmentManager.beginTransaction()
                    .replace(R.id.event_details_container, detailsfragment).commit();
        }
    }
}
