package es.uniovi.myapplication.Vista;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import es.uniovi.myapplication.R;

public class EventListAdapter extends RecyclerView.Adapter<EventListAdapter.EventViewHolder> {

    private  LayoutInflater mlayoutInflater;
    private List<String> mevents;
    private ListItemOnClickInterface listItemOnClickInterface;

    public class EventViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener{
        private TextView mEventName;

        public EventViewHolder(View itemView) {
            super(itemView);
            mEventName = itemView.findViewById(R.id.nameTextView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listItemOnClickInterface.onItemClick(this.mEventName.getText().toString());
        }
    }

    public EventListAdapter(Context context) {
        this.mlayoutInflater = LayoutInflater.from(context);
        if (context instanceof ListItemOnClickInterface)
            listItemOnClickInterface = (ListItemOnClickInterface) context;
    }


    public void setNames(List<String> mcourses) {
        this.mevents = mcourses;
        notifyDataSetChanged();
    }


    @Override
    public EventListAdapter.EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mlayoutInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new EventViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EventListAdapter.EventViewHolder holder, int position) {
        if (mevents != null) {holder.mEventName.setText(mevents.get(position));}
        else {holder.mEventName.setText("Sin evento");}
    }

    @Override
    public int getItemCount() {
        if (mevents != null)
            return mevents.size();
        else
            return 1;
    }
}
