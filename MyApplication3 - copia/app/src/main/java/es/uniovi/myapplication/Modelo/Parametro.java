package es.uniovi.myapplication.Modelo;

import com.google.gson.annotations.SerializedName;

//Clase que nos posibilita hacer la deseriazliacion del json de forma comoda
public class Parametro {
    @SerializedName("rdfs_comment")
    Coment coment;
    class Coment{
        public String getComm() {
            return this.comment;
        }
        @SerializedName("value")
        String comment;
    }

    @SerializedName("geo_long")
    Long lon;
    class Long{
        public String getlon() {
            return this.lon;
        }
        @SerializedName("value")
        String lon;
    }

    @SerializedName("geo_lat")
    Lat lat;
    class Lat{
        public String getlat() {
            return this.lat;
        }
        @SerializedName("value")
        String lat;
    }

    @SerializedName("om_seccionProcedencia")
    Proc proc;
    class Proc{
        public String getproc() {
            return this.proc;
        }
        @SerializedName("value")
        String proc;
    }

    @SerializedName("om_horarioEvento")
    Heve heve;
    class Heve{
        public String getheve() {
            return this.heve;
        }
        @SerializedName("value")
        String heve;
    }

    @SerializedName("om_categoriaEvento")
    Ceve ceve;
    class Ceve{
        public String getceve() {
            return this.ceve;
        }
        @SerializedName("value")
        String ceve;
    }

    @SerializedName("rdfs_label")
    Label label;
    class Label{
        public String getlabel() {
            return this.label;
        }
        @SerializedName("value")
        String label;
    }

    public String getDescripcion(){
        if(coment == null)
            return "nada";
        return coment.getComm();
    }

    public double getLong(){
        if(lon == null)
            return 1;
        return Double.parseDouble(lon.getlon());
    }

    public double getLat(){
        if(lat == null)
            return 1.0;
        return Double.parseDouble(lat.getlat());
    }

    public String getSeccion(){
        if(proc == null)
            return "nada";
        return proc.getproc();
    }

    public String getHorario(){
        if(heve == null)
            return "nada";
        return heve.getheve();
    }

    public String getCategoria(){
        if(ceve == null)
            return "nada";
        return ceve.getceve();
    }

    public String getTitulo(){
        if(label == null)
            return "nada";
        return label.getlabel();
    }

}

