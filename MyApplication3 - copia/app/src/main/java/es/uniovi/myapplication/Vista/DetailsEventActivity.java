package es.uniovi.myapplication.Vista;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import es.uniovi.myapplication.R;

public class DetailsEventActivity extends AppCompatActivity {

    public static final String NAME_DETAIL = "es.uniovi.myapplication.Vista.name_detail";
    EventDetailsFragment fragment;

    //Se crea el fragmento y se lanza
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        String description = intent.getStringExtra(NAME_DETAIL);
        setContentView(R.layout.event_details_activity);

        if (findViewById(R.id.fragment_container) != null) {

            // Si estamos restaurando desde un estado previo no hacemos nada
            if (savedInstanceState != null) {
                return;
            }
            // Crear el fragmento pasándole el parámetro
            fragment = EventDetailsFragment.newInstance(description);

            // Añadir el fragmento al contenedor 'fragment_container'
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, fragment).commit();
        }
    }



}
