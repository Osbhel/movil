package es.uniovi.myapplication.Modelo;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import es.uniovi.myapplication.Modelo.Event;

@Dao
public interface EventDAO {
    @Insert
    public void insertEvent(Event event);

    @Query("DELETE from event_table")
    public void deleteAll();

    @Query("SELECT * FROM event_table WHERE titulo LIKE :name")
    public LiveData<Event> getEventByName(String name);

    @Query("SELECT titulo FROM event_table")
    public LiveData<List<String>> getNames();

    @Query("SELECT titulo FROM event_table WHERE categoria LIKE :name")
    public LiveData<List<String>> getEventByFiltro(String name);



}
