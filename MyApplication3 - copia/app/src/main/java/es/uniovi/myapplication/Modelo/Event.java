package es.uniovi.myapplication.Modelo;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "event_table")
public class Event {
    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    @PrimaryKey
    @NonNull
    private  int id;

    private String titulo;
    private String categoria;
    private String description;
    private String seccion;
    private double latitud;
    private double longitud;
    private String horario;


    public Event(int id, String titulo, String categoria, String description, String seccion, double latitud,
                 double longitud, String horario) {
        this.id=id;
        this.titulo = titulo;
        this.categoria = categoria;
        this.description = description;
        this.seccion = seccion;
        this.latitud=latitud;
        this.longitud = longitud;
        this.horario = horario;
    }


    public String getTitulo() {
        return titulo;
    }

    public String getCategoria() {
        return categoria;
    }

    public String getDescription() {
        return description;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getSeccion() {
        return this.seccion;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setSeccion(String seccion) {
       this.seccion = seccion;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }
}
