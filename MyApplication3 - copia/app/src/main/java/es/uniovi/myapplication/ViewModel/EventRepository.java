package es.uniovi.myapplication.ViewModel;


import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

import es.uniovi.myapplication.Modelo.Event;
import es.uniovi.myapplication.Modelo.EventDAO;
import es.uniovi.myapplication.Modelo.EventDatabase;

public class EventRepository {
    private EventDAO eventDAO;
    private LiveData<List<String>> mNames;

    public EventRepository(Application application) {
        this.eventDAO = EventDatabase.getDatabase(application).eventDAO();
        this.mNames = this.eventDAO.getNames();

    }

    public LiveData<Event> getEventByName(String name) {
        return eventDAO.getEventByName(name);
    }

    public LiveData<List<String>> getEventByFiltro(String name) {
        return eventDAO.getEventByFiltro(name);
    }

    public LiveData<List<String>> getEventNames() {
        return mNames;
    }

    public void insertEvent(Event event) {
        new insertAsyncTask(eventDAO).execute(event);
    }

    public void deleteAll() {
        new deleteAllAsyncTask(eventDAO).execute();
    }

    private static class insertAsyncTask extends AsyncTask<Event, Void, Void> {

        private EventDAO myAsyncTaskDAO;

        public insertAsyncTask(EventDAO myAsyncTaskDAO) {
            this.myAsyncTaskDAO = myAsyncTaskDAO;
        }

        @Override
        protected Void doInBackground(Event... cours) {
            myAsyncTaskDAO.insertEvent(cours[0]);
            return null;
        }
    }

    private static class deleteAllAsyncTask extends AsyncTask<Void, Void, Void> {

        private EventDAO myAsyncTaskDAO;

        public deleteAllAsyncTask(EventDAO myAsyncTaskDAO) {
            this.myAsyncTaskDAO = myAsyncTaskDAO;
        }

        @Override
        protected Void doInBackground(Void ...voids) {
            myAsyncTaskDAO.deleteAll();
            return null;
        }
    }
}
