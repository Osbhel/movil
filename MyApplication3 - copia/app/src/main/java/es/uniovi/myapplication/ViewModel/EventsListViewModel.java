package es.uniovi.myapplication.ViewModel;

import android.app.Application;
import android.arch.core.util.Function;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.support.annotation.NonNull;

import es.uniovi.myapplication.Modelo.Event;


public class EventsListViewModel extends AndroidViewModel {
    private EventRepository eventRepository;
    private LiveData<Event> mevent;
    private MutableLiveData<String> mName;


    public void setName(String mName) {
        this.mName.setValue(mName);
    }

    public LiveData<Event> getEvent() {
        return this.mevent;
    }

    public EventsListViewModel(@NonNull Application application) {
        super(application);
        eventRepository = new EventRepository(application);
        mName = new MutableLiveData<>();
        mevent = Transformations.switchMap(mName, new Function<String, LiveData<Event>>() {
            @Override
            public LiveData<Event> apply(String input) {
                return eventRepository.getEventByName(input);
            }
        });




    }
}
