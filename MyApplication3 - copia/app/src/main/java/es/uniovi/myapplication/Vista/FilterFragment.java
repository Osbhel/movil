package es.uniovi.myapplication.Vista;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;

import es.uniovi.myapplication.Modelo.Event;
import es.uniovi.myapplication.R;

public class FilterFragment extends Fragment implements View.OnClickListener {
    //Fragmento correspoondiente al filtro
    Spinner spiner;
    public static FilterFragment newInstance(){
        FilterFragment fragment = new FilterFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View viewRoot;
        viewRoot = inflater.inflate(R.layout.activity_filter, container,false);
        Button boton= viewRoot.findViewById(R.id.boton1);
        spiner = viewRoot.findViewById(R.id.spinner);
        boton.setOnClickListener(this);
        return viewRoot;
    }


    @Override
    public void onClick(View view) {
        Intent intent = new Intent();
        // Comprabmos que valor es seleccionado en el filtro y se construye el bundle y se
        // finaliza la actividad para que retorne los resultados a la  que le ha llamado
        intent.putExtra("Filtro",spiner.getSelectedItem().toString());
        getActivity().setResult(getActivity().RESULT_OK,intent);
        getActivity().finish();
    }
}
