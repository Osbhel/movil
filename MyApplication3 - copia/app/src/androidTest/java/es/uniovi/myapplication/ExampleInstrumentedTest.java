package es.uniovi.myapplication;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObject2;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.*;


@RunWith(AndroidJUnit4.class)

public class ExampleInstrumentedTest {

    private static final String PACKAGEMIO="es.uniovi.myapplication";
    private static final int LAUNCH_TIMEOUT= 5000;
    private UiDevice mDevice;

    @Before
    public void startMainActivityFromHomeScreen(){
        // Inticializamos la UIDevice Instance
        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        // Presionamos el boton home para estar en la pantalla principal
        mDevice.pressHome();

        // esperamos por el launcher
        final String launcherPackage = getLauncherPackageName();
        assertThat(launcherPackage, notNullValue());
        mDevice.wait(Until.hasObject(By.pkg(launcherPackage).depth(0)), LAUNCH_TIMEOUT);

        Context context = InstrumentationRegistry.getContext();
        final Intent intent = context.getPackageManager()
                .getLaunchIntentForPackage(PACKAGEMIO);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);

        // lanzamos la aplicacion deseada y esperamos hasta que este lista en este caso lanzamos la play store
        mDevice.wait(Until.hasObject(By.pkg(PACKAGEMIO).depth(0)), LAUNCH_TIMEOUT);
    }

    @Test
    public void comprobarPantallaFiltrar() throws Exception {
        //Seleccionamos el boton opciones de la toolbar
        new UiObject(new UiSelector().className("android.widget.ImageView")).click();
        //Seleccionamos el boton Filtrar
        new UiObject(new UiSelector().className("android.widget.ListView")).click();

        UiObject2 boton =mDevice.wait(Until.findObject(By.res(PACKAGEMIO,
                "boton1")),500);

        assertThat(boton.getText(), is(equalTo("FILTRAR")));

    }

    @Test
    public void comprobaFiltrar() throws Exception {
        //Seleccionamos el boton opciones de la toolbar
        new UiObject(new UiSelector().className("android.widget.ImageView")).click();
        //Seleccionamos el boton Filtrar
        new UiObject(new UiSelector().className("android.widget.ListView")).click();

        UiObject2 boton =mDevice.wait(Until.findObject(By.res(PACKAGEMIO,
                "boton1")),500);

        boton.click();

        UiObject2 text = mDevice.wait(Until.findObject(By.res(PACKAGEMIO,"nameTextView")),
                500);

        text.click();

        UiObject2 categoria = mDevice.wait(Until.findObject(By.res(PACKAGEMIO,"details_teacher")),
                500);

        assertThat(categoria.getText(), is(equalTo("Otros")));

    }



    private String getLauncherPackageName() {
        final Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);

        PackageManager pm = InstrumentationRegistry.getContext().getPackageManager();
        ResolveInfo resolveInfo = pm.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return resolveInfo.activityInfo.packageName;
    }
}
